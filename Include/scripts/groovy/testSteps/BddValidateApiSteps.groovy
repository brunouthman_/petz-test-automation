package testSteps
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.JsonSlurper


class BddValidateApiSteps {
	ResponseObject response;

	@Given("I want to test the API")
	def I_want_to_test_the_api() {
		println("I want to test the API")
	}

	@When("I request the API (.*)")
	def I_request_the_api(String apiName) {

		if(apiName=="currentWeatherData"){
			this.response = WS.sendRequest(findTestObject('Object Repository/apis/current-weather-data-api/current-weather-data-api-request'))
			WS.verifyResponseStatusCode(this.response, 200)
		}
	}

	@Then("I verify the successful response")
	def I_verify_the_successful_response() {

		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(this.response.getResponseText())
		String region = parsedJson.name
		String temp = parsedJson.main.temp
		String description = parsedJson.weather[0].description

		println("===========WEATHER===============")
		println("region: "+region)
		println("weather: "+temp)
		println("description: "+description)
		println("===================================")
	}
}