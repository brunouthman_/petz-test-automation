package testSteps
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

class BddValidateAutomationSteps {

	@Given("I open the browser")
	def I_open_the_browser() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
	}


	@Given("I navigate to home page")
	def I_navigate_to_home_page() {
		WebUI.navigateToUrl(GlobalVariable.urlPetzBlog)
	}


	@When("I click on (.*)")
	def I_click_on_menu(String menu) {

		switch(menu){
			case "petzComBr":
				WebUI.waitForElementVisible(findTestObject('pages/home-page/li_menu_petzComBr'), 5)
				WebUI.click(findTestObject('pages/home-page/li_menu_petzComBr'))
				break
			case "nossasLojas":
				WebUI.waitForElementVisible(findTestObject('pages/home-page/li_menu_nossasLojas'), 5)
				WebUI.click(findTestObject('pages/home-page/li_menu_nossasLojas'))
				break
			case "atendimento":
				WebUI.waitForElementVisible(findTestObject('pages/home-page/li_menu_atendimento'), 5)
				WebUI.click(findTestObject('pages/home-page/li_menu_atendimento'))
				break
			case "mapaDoSite":
				WebUI.waitForElementVisible(findTestObject('pages/home-page/li_menu_mapaDoSite'), 5)
				WebUI.click(findTestObject('pages/home-page/li_menu_mapaDoSite'))
				break
			case "blackFridayPetz":
				WebUI.waitForElementVisible(findTestObject('pages/home-page/li_menu_blackFridayPetz'), 5)
				WebUI.click(findTestObject('pages/home-page/li_menu_blackFridayPetz'))
		}
	}


	@When("I verify (.*) page opened")
	def I_verify_menu_page_opened(String page) {

		switch(page){
			case "petzComBr":
				WebUI.waitForElementVisible(findTestObject('pages/petz-page/div_login_ou_cadastre_se'), 5)
				WebUI.verifyElementVisible(findTestObject('pages/petz-page/div_login_ou_cadastre_se'))
				break
			case "nossasLojas":
				WebUI.waitForElementVisible(findTestObject('pages/petz-page/h1_conheca_nossas_lojas'), 5)
				WebUI.verifyElementVisible(findTestObject('pages/petz-page/h1_conheca_nossas_lojas'))
				break
			case "atendimento":
				WebUI.waitForElementVisible(findTestObject('pages/petz-page/iframe_fale_conosco'), 5)
				WebUI.verifyElementVisible(findTestObject('pages/petz-page/iframe_fale_conosco'))
				break
			case "mapaDoSite":
				WebUI.waitForElementVisible(findTestObject('pages/petz-page/h1_mapa_do_site'), 5)
				WebUI.verifyElementVisible(findTestObject('pages/petz-page/h1_mapa_do_site'))
				break
			case "blackFridayPetz":
				WebUI.waitForElementVisible(findTestObject('pages/petz-page/span_contagem_regressiva'), 5)
				WebUI.verifyElementVisible(findTestObject('pages/petz-page/span_contagem_regressiva'))
		}
		WebUI.back()
	}


	@Then("I close the browser")
	def I_close_the_browser() {
		WebUI.closeBrowser()
	}
}