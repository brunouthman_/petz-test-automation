Feature: Validate BDDs automation

  Scenario Outline: Validate menu header
    Given I open the browser
    And I navigate to home page
    When I click on <menu1>
    And I verify <menu1> page opened
    And I click on <menu2>
    And I verify <menu2> page opened
    And I click on <menu3>
    And I verify <menu3> page opened
    And I click on <menu4>
    And I verify <menu4> page opened
    And I click on <menu5>
    And I verify <menu5> page opened
    Then I close the browser

    Examples: 
      | menu1  				 |menu2					  |menu3					 |menu4						|menu5									|
      | petzComBr  |nossasLojas		|atendimento|mapaDoSite   |blackFridayPetz 	|
