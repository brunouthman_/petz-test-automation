<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_login_ou_cadastre_se</name>
   <tag></tag>
   <elementGuidId>62e2a415-1dda-426e-b78d-42bbf33e6f5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'dropbtn' and (text() = 'Faça seu login ou cadastre-se' or . = 'Faça seu login ou cadastre-se')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropbtn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Faça seu login ou cadastre-se</value>
   </webElementProperties>
</WebElementEntity>
